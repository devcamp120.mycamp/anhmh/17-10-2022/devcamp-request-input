//Câu lệnh này tương tự import 'express'; Dùng để import thư viện vào project
const { response } = require("express");
const express = require("express");
const { request } = require("http");

//Khởi tạo express
const app = express();

//Cấu hình để app đọc được body request dạng json
app.use(express.json());
//Cấu hình để app đọc được tiếng Việt
app.use(express.urlencoded({
    extended: true
}))

//Khai báo cổng của project
const port = 8000;

//Khai báo API dạng GET "/" sẽ chạy vào đây
//Callback function là một tham số của hàm khác và nó sẽ được thực thi ngay sau khi hàm đấy được gọi
app.get("/", (request, response) => {
    let today = new Date();

    response.status(200).json({
        message: `Xin chào! Hôm nay là ngày ${today.getDate()} tháng ${today.getMonth()} năm ${today.getFullYear()}`
    })
})

app.get("/request-params/:param1", (request, response) => {
    let param1 = request.params.param1;
    
    response.status(200).json({
        param1: param1
    })
})

app.get("/request-query", (request, response) => {
    let query = request.query;

    response.status(200).json({
        queryRequest: query
    });
})

app.post("/request-body", (request, response) => {
    let body = request.body;

    response.status(200).json({
        bodyRequest: body
    })
})

//Chạy app express
app.listen(port, () => {
    console.log("App listening on port (Ứng dụng đang chạy trên cổng) " + port);
})